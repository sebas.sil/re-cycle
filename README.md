# RE-CYLE

Projeto para mapear os pontos de coleta de lixo reciclavel

# Get started

Execute o comando abaixo para baixar as dependencias do projeto
    
    `yarn install`

Execute o comando abaixo para criar o banco de dados (de acordo com o arquivo de configuracao (`connection.ts`)[src/database/connection.ts])

    `yarn knex:migrate`

Execute o comando abaixo para popular o banco com as informações iniciais necessárias

    `yarn knex:seed`

## Built With

* [VS Code 1.45.1](https://code.visualstudio.com) - IDE de desenvolvimento
* [Ivy 1.22.4](https://yarnpkg.com) - Utilizado como gerenciador de dependências
