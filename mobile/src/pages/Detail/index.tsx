import React, { useEffect, useState } from 'react'
import { View, StyleSheet, TouchableOpacity, Image, Text, SafeAreaView, Linking, ScrollView } from 'react-native'
import { Feather as Icon, FontAwesome } from '@expo/vector-icons'
import { useNavigation, useRoute } from '@react-navigation/native'
import { RectButton } from 'react-native-gesture-handler'
import { apiinternal } from '../../services/api'
import { SvgUri } from 'react-native-svg'

interface Parms {
  point: number
}
interface Point {
  id: number,
  image: string,
  name: string,
  email: string,
  phone: string,
  //city: string,
  //uf: string,
  //latitude: number,
  //longitude: number,
  items: number[]
}
interface Item {
  id: number,
  title: string,
  image: string
}

const Detail = () => {

  const navigation = useNavigation()
  const route = useRoute()

  const [point, setPoint] = useState<Point>()
  const [items, setItems] = useState<Item[]>([])

  const routeParms = route.params as Parms

  useEffect(() => {
    apiinternal.get(`points/${routeParms.point}`).then(res => {
      setPoint(res.data)
    })
  }, [])

  useEffect(() => {
    apiinternal.get('items').then(res => {
      setItems(res.data)
    })
  }, [])

  if (!point) {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
      </View>
    )
  }

  function handleNavigationBack() {
    navigation.goBack()
  }
  function handleComposeMail() {
    Linking.openURL(`mailto:${point?.email}?Subject=Interesse na coleta de residuos`)
  }
  function handleComposeWhatsapp() {
    Linking.openURL(`whatsapp://send?phone=${point?.phone}&text=Interesse na coleta de residuos`)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <TouchableOpacity onPress={handleNavigationBack}>
          <Icon name='arrow-left' size={20} color='#34cb79' />
        </TouchableOpacity>

        <Image style={styles.pointImage} source={{ uri: 'http://192.168.25.11:3333/images/' + point.image }} />
        <Text style={styles.pointName}>{point.name}</Text>

        <Text style={styles.pointItems}>Itens Coletados neste local</Text>
        <View style={styles.itemsContainer}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 20 }}>
        {items.filter(item => point.items.includes(item.id)).map(item => (
            <TouchableOpacity key={String(item.id)} activeOpacity={0.6} style={[styles.item]}>
              <SvgUri width={42} height={42} uri={'http://192.168.25.11:3333/images/' + item.image} />
              <Text style={styles.itemTitle}>{item.title}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>

        

        <View style={styles.address}>
          <Text style={styles.addressTitle}>Endereço</Text>
          <Text style={styles.addressContent}>{point.name}</Text>
        </View>
      </View>

      <View style={styles.footer}>
        <RectButton style={styles.button} onPress={handleComposeWhatsapp}>
          <FontAwesome name='whatsapp' size={20} color='#fff' />
          <Text style={styles.buttonText}>Whatsapp</Text>
        </RectButton>

        <RectButton style={styles.button} onPress={handleComposeMail}>
          <Icon name='mail' size={20} color='#fff' />
          <Text style={styles.buttonText}>E-mail</Text>
        </RectButton>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 32,
    paddingTop: 20,
  },

  pointImage: {
    width: '100%',
    height: 120,
    resizeMode: 'cover',
    borderRadius: 10,
    marginTop: 32,
  },

  itemsContainer: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 32
  },

  item: {
    backgroundColor: '#fff',
    borderWidth: 2,
    borderColor: '#eee',
    height: 120,
    width: 120,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 16,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
  },

  itemTitle: {
    fontFamily: 'Roboto_400Regular',
    textAlign: 'center',
    fontSize: 13,
  },

  pointName: {
    color: '#322153',
    fontSize: 28,
    fontFamily: 'Ubuntu_700Bold',
    marginTop: 24,
    marginBottom: 24
  },

  pointItems: {
    fontFamily: 'Roboto_400Regular',
    fontSize: 16,
    lineHeight: 24,
    marginTop: 8,
    color: '#6C6C80'
  },

  address: {
    marginTop: 32,
  },

  addressTitle: {
    color: '#322153',
    fontFamily: 'Roboto_500Medium',
    fontSize: 16,
  },

  addressContent: {
    fontFamily: 'Roboto_400Regular',
    lineHeight: 24,
    marginTop: 8,
    color: '#6C6C80'
  },

  footer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: '#999',
    paddingVertical: 20,
    paddingBottom: 20,
    paddingHorizontal: 32,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  button: {
    width: '48%',
    backgroundColor: '#34CB79',
    borderRadius: 10,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText: {
    marginLeft: 8,
    color: '#FFF',
    fontSize: 16,
    fontFamily: 'Roboto_500Medium',
  },
})

export default Detail