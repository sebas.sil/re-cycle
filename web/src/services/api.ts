import axios from 'axios'

const apiinternal = axios.create({
    baseURL: 'http://localhost:3333'
})

const apiexternal = axios.create({
    baseURL: 'https://servicodados.ibge.gov.br/api/v1/localidades'
})

export { apiinternal, apiexternal }