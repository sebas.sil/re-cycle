import React, { useEffect, useState, ChangeEvent, FormEvent } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { apiexternal, apiinternal } from '../../services/api'
import { LeafletMouseEvent } from 'leaflet'

import './style.css'
import logo from '../../assets/logo.svg'
import { FiArrowLeft } from 'react-icons/fi'
import { Map, TileLayer, Marker } from 'react-leaflet'
import Dropzone from '../components/Dropzone'

interface Item {
    id: number,
    title: string,
    image: string
}
interface Estado {
    id: number,
    sigla: string,
    nome: string
}
interface Cidade {
    id: number,
    nome: string
}

const CreatePoint = () => {

    const [items, setItems] = useState<Item[]>([])
    const [states, setStated] = useState<Estado[]>([])
    const [cities, setCities] = useState<Cidade[]>([])

    const [selectedUF, setSelectedUF] = useState<string>()
    const [selectedCity, setSelectedCity] = useState<string>()

    const [selectedPosition, setSelectedPosition] = useState<[number, number]>([0, 0])
    const [initialPosition, setInitialPosition] = useState<[number, number]>([0, 0])

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        phone: ''
    })

    const [selectedItems, setSelectedItems] = useState<number[]>([])
    const history = useHistory()
    
    const [selectedFile, setSelectedFile] = useState<File>()

    useEffect(() => {
        apiinternal.get('items').then(res => {
            setItems(res.data)
        })
    }, []) //disparado apenas uma vez ja q nao foi passado o componente que deve ser monitorado

    useEffect(() => {
        apiexternal.get('estados?orderBy=nome').then(res => {
            setStated(res.data)
        })
    }, [])

    useEffect(() => {
        if (selectedUF) {
            apiexternal.get(`/estados/${selectedUF}/municipios?orderBy=nome`).then(res => {
                setCities(res.data)
            })
        }
    }, [selectedUF])

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords
            setInitialPosition([latitude, longitude])
        })
    }, [])

    function handleSelectUF(event: ChangeEvent<HTMLSelectElement>) {
        setSelectedUF(event.target.value)
    }
    function handleSelectCity(event: ChangeEvent<HTMLSelectElement>) {
        setSelectedCity(event.target.value)
    }
    function handleMapClick(event: LeafletMouseEvent) {
        setSelectedPosition([event.latlng.lat, event.latlng.lng])
    }
    function handleInputChange(event: ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target
        setFormData({ ...formData, [name]: value }) //[] para informar o nome da propriedade como sendo o proprio nome da variavel
    }
    function handleSelectedItem(id: number) {
        if (selectedItems.includes(id)) {
            const items = selectedItems.filter(item => item !== id);
            setSelectedItems(items)
        } else {
            setSelectedItems([...selectedItems, id])
        }
    }
    async function handleSubmit(event: FormEvent) {
        event.preventDefault()

        const data = new FormData()
        
        data.append('name', formData.name)
        data.append('email', formData.email)
        data.append('phone', formData.phone)
        if(selectedCity){
            data.append('city', selectedCity)
        }
        if(selectedUF){
            data.append('uf', selectedUF)
        }
        data.append('latitude', String(selectedPosition[0]))
        data.append('longitude', String(selectedPosition[1]))
        data.append('items', selectedItems.join(','))
        if(selectedFile){
            data.append('image', selectedFile)
        }

        await apiinternal.post('points', data)

        alert('ponto de coleta cadastrado')

        history.push('/')
    }

    return (
        <div id='page-create-point'>
            <header>
                <img src={logo} alt="re-cyle" />
                <Link to='/' >
                    <FiArrowLeft />
                    Voltar para a home
                </Link>
            </header>
            <main>
                <form onSubmit={handleSubmit}>
                    <h1>Cadastro do ponto de coleta</h1>

                    <Dropzone onFileUploaded={setSelectedFile}/>

                    <fieldset>
                        <legend>
                            <h2>Dados</h2>
                        </legend>

                        <div className='field'>
                            <label htmlFor='name'>Nome da entidade</label>
                            <input id='name' name='name' type='text' onChange={handleInputChange}></input>
                        </div>

                        <div className="field-group">
                            <div className='field'>
                                <label htmlFor='name'>Email</label>
                                <input id='email' name='email' type='email' onChange={handleInputChange}></input>
                            </div>
                            <div className='field'>
                                <label htmlFor='name'>Telefone</label>
                                <input id='phone' name='phone' type='text' onChange={handleInputChange}></input>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset>
                        <legend>
                            <h2>Endereço</h2>
                            <span>Selecione o endereço no mapa</span>
                        </legend>

                        <Map center={initialPosition} zoom={15} onClick={handleMapClick}>
                            <TileLayer
                                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />

                            <Marker position={selectedPosition} />
                        </Map>

                        <div className="field-group">
                            <div className="field">
                                <label htmlFor="uf">Estado (UF)</label>
                                <select name="uf" id="uf" value={selectedUF} onChange={handleSelectUF}>
                                    <option>Selecione um estado</option>
                                    {states.map(state => (
                                        <option key={state.id} value={state.sigla}>{state.nome}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="field">
                                <label htmlFor="city">Cidade</label>
                                <select name="city" id="city" value={selectedCity} onChange={handleSelectCity}>
                                    <option>Selecione uma cidade</option>
                                    {cities.map(city => (
                                        <option key={city.id} value={city.nome}>{city.nome}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>
                            <h2>Itens de coleta</h2>
                            <span>Selecione um ou mais itens de coleta</span>
                        </legend>

                        <ul className="items-grid">
                            {items.map(item => (
                                <li key={item.id} onClick={() => handleSelectedItem(item.id)} className={selectedItems.includes(item.id) ? 'selected' : ''}>
                                    <img src={'http://localhost:3333/images/' + item.image} alt={item.title} />
                                    <span>{item.title}</span>
                                </li>
                            ))}
                        </ul>
                    </fieldset>

                    <button type='submit'>Cadastrar ponto de coleta</button>
                </form>
            </main>
        </div>
    )
}

export default CreatePoint