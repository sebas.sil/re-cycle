import knex from 'knex'

const knexConfig = require('../config/knexconfig')
const connection = knex(knexConfig)

module.exports = connection
