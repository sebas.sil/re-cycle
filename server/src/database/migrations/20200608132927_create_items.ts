import Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable('items', table => {
        table.comment('Tabela de itens que um ponto de coleta pode receber'),
        table.increments('id').primary().comment('identificador unico da tabela items'),
        table.string('image').notNullable().comment('nome da imagem para mostrar na descricao do ponto de coleta'),
        table.string('title').notNullable().comment('nome do item de coleta'),
        table.string('description').comment('descricao do item de coleta')
    })
}


export async function down(knex: Knex): Promise<any> {
    knex.schema.dropTable('items')
}

