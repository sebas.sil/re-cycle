import Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable('points_items', table => {
        table.comment('Tabela associativa entre pontos e itens, itendifica quais itens um pontos de coleta recebe'),
        table.integer('item').notNullable().references('id').inTable('items').comment('id do item de coleta'),
        table.integer('point').notNullable().references('id').inTable('points').comment('id do ponto de coleta')
    })
}


export async function down(knex: Knex): Promise<any> {
    knex.schema.dropTable('points_items')
}

