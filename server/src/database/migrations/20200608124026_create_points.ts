import Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTable('points', table => {
        table.comment('Tabela de pontos de coleta com suas coordenadas'),
        table.increments('id').primary().comment('identificador unico da tabela points'),
        table.string('image').notNullable().comment('caminho da imagem para mostrar no mapa'),
        table.string('name').notNullable().comment('nome do ponto de coleta')
        table.string('email').notNullable().comment('e-mail de contato com o ponto de coleta')
        table.string('phone').notNullable().comment('contato telefonico com o ponto de coleta')
        table.string('city').notNullable().comment('cidade na qual reside o ponto de coleta')
        table.string('uf', 2).notNullable().comment('estado no qual o ponto de coleta resite')
        table.decimal('latitude').notNullable().comment('latitude do ponto de coleta')
        table.decimal('longitude').notNullable().comment('longitude do ponto de coleta')
    })
}


export async function down(knex: Knex): Promise<any> {
    knex.schema.dropTable('points')
}

