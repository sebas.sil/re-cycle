const multer = require('multer')
const path = require('path')

const multerConfig = {
    storage: multer.diskStorage({
        destination: path.resolve(__dirname, '..', '..', 'uploads'),
        filename:(req, file, call) => {
            const timestamp = process.hrtime()
            const filename = '' + timestamp[0] + timestamp[1] + path.extname(file.originalname)
            call(null, filename)
        }
    })
}

module.exports = multerConfig