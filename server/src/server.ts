import express from 'express'
import cors from 'cors'
import usrroutes from './modules/users.routes.js'
import itmroutes from './modules/items.routes.js'
import ptnroutes from './modules/points.routes.js'
import { errors } from 'celebrate'

const app = express()
app.use(cors())
app.use(express.json())
app.use(usrroutes)
app.use(itmroutes)
app.use(ptnroutes)

app.use('/images', express.static('images'))
app.use('/images', express.static('uploads'))

app.use(errors())

app.listen(3333)