const express = require('express')
const routes = express.Router()

const ItemController = require('../controllers/items.controller')
const itemCtrl = new ItemController()

routes.get('/items', itemCtrl.index)
routes.get('/items/:id', itemCtrl.show)

module.exports = routes