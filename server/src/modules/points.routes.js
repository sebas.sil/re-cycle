const express = require('express')
const routes = express.Router()
const PointController = require('../controllers/points.controller')
const multerConfig = require('../config/multer')
const multer = require('multer')
const { celebrate, Joi } = require('celebrate')

const upload = multer(multerConfig)
const pointCtrl = new PointController()

routes.post('/points', upload.single('image'), celebrate({
    body: Joi.object().keys({
        nome: Joi.string().required(),
        email: Joi.string().required().email(),
        phone: Joi.number().required(),
        latitude: Joi.number().required(),
        longitude: Joi.number().required(),
        city: Joi.string().required(),
        uf: Joi.string().required().max(2),
        items: Joi.string().required()
    })
}, {
    abortEarly: false
}), pointCtrl.create)
routes.get('/points/:id', pointCtrl.show)
routes.get('/points', pointCtrl.index)

module.exports = routes