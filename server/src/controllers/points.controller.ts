import { Request, Response } from 'express'
import { MulterRequest } from 'multer'
const knex = require('../database/connection')

class PointController {

    async create(req: MulterRequest, res: Response) {
        try {
            const { id, name, image, email, phone, city, uf, latitude, longitude, items } = req.body
            console.log('/points found')

            // garante a erificacao das FKs no sqlite
            await knex.raw('PRAGMA foreign_keys = ON');
            const trx = await knex.transaction();

            const point = {
                name, image: req.file.filename, email, phone, city, uf, latitude:Number(latitude), longitude:Number(longitude)
            }

            // insere um ponto de coleta mantendo a transacao
            const insertedIds = await trx('points').insert(point)

            if (items) {
                // se conseguiu inserir o ponto de coleta, insira os itens que este ponto trabalha
                const points_itens = items.split(',').map((item_id:number) => {
                    return {
                        point: insertedIds[0],
                        item: item_id
                    }
                })

                await trx('points_items').insert(points_itens)
            }

            // se tudo deu certo, grave e encerre
            trx.commit()
            res.json({ ...point, id: insertedIds[0] })

        } catch (e) {
            console.log(e)
            res.status(500).send()
        }
    }

    async show(req: Request, res: Response) {
        try {
            const id = req.params.id

            const point = await knex('points').select('*').where('id', id).first()
            if (point) {
                const items = await knex('items')
                    .join('points_items', 'items.id', '=', 'points_items.item')
                    .groupBy('items.id')
                    .where('points_items.point', id)
                    .select('items.id')
                
                point.items = items.map(item => item.id)
                res.json(point)
            } else {
                res.json({})
            }
        } catch (error) {
            console.log(error)
            res.status(500).send()
        }
    }

    async index(req: Request, res: Response) {

        const { city, uf, items } = req.query

        try {
            console.log('get all points', { city, uf, items })

            const query = knex('points').join('points_items', 'points.id', '=', 'points_items.point')

            if (items) {
                let itemsa = String(items).split(',').map(i => Number(i.trim()))
                query.whereIn('points_items.item', itemsa)
            }

            if (city) {
                query.where('points.city', String(city))
            }

            if (uf) {
                query.where('uf', String(uf))
            }

            const points = await query.distinct().select('points.*')

            res.json(points)
        } catch (error) {
            console.log(error)
            res.status(500).send()
        }
    }
}

module.exports = PointController