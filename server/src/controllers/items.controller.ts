import { Request, Response } from 'express'
const knex = require('../database/connection')

class ItemController {
    async index(req: Request, res: Response) {
        try {
            //console.log('/items found')
            const items = await knex('items').select('*')
            res.json(items)
        } catch (e) {
            console.log(e)
            res.status(500).send()
        }
    }

    async show(req: Request, res: Response) {
        try {
            const id = req.params.id

            //console.log('/item found', id)
            const item = await knex('items').select('*').where('id', id).first()
            res.json(item)
        } catch (e) {
            console.log(e)
            res.status(500).send()
        }
    }
}

module.exports = ItemController